# Dependency scanner

- [Introduction](#introduction)
- [Installation](#installation)
- [Usage](#usage)
- [Tests](#tests)

Introduction
============

This project is a webapp based on Django that scans the dependencies of a project for vulnerabilities.

As of now, it is only capable of parsing Gem files and pip requirements files. After scanning a git repository, this project will produce a scan report, showing which of your dependencies have vulnerabilities (CVEs and / or exploits from ExploitDB).

To help you track progress over time, a new scan of your repository is scheduled everyday at midnight.

Vulnerability databases used: CIRCL public REST API and ExploitDB

Installation
============

## a. With Ansible

If you want to deploy this project to a server (or any virtual machine), you can use the playbook created for that purpose:
[https://gitlab.com/Guillaume695/dependency-scanner-ansible](https://gitlab.com/Guillaume695/dependency-scanner-ansible)

Follow the instructions in the README of that repository.

## b. Manually

You can also install this project manually by following these few steps.

*(Tested with a debian/stretch64 Vagrant Virtual Machine)*

#### 1. Make sure you have python2.7, pip, git and mysql installed

```bash
$ apt-get install python python-pip git mysql-server python-mysqldb
```

#### 2. This project uses ExploitDB to search for exploits. Follow the instructions on ExploitDB's website to install searchsploit.
[https://www.exploit-db.com/searchsploit/](https://www.exploit-db.com/searchsploit/)

#### 3. Clone this repository

```bash
$ git clone https://gitlab.com/Guillaume695/dependency-scanner
```

#### 4. Install this project's dependencies

```bash
$ cd dependency-scanner/
$ pip install -r requirements.txt
```
Make sure yolk is available from the command line. You might have to add the path to its binary to your $PATH variable by doing:

```bash
$ PATH=$PATH:~/.local/bin
```

#### 5. Create a MySQL database and a MySQL user for that database. Here is an example of what your SQL script might look like.

```sql
CREATE DATABASE my_database_name;
CREATE USER 'my_username'@'localhost' IDENTIFIED BY 'my_password';
GRANT USAGE ON *.* TO 'my_username'@'localhost' IDENTIFIED BY 'my_password' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
GRANT ALL PRIVILEGES ON `my_database_name`.* TO 'my_username'@'localhost';
```
Then execute your script with the following command:
```bash
$ sudo mysql < my_sql_script
```

#### 6. Update the database settings in depscan/settings.py with the database name and user you've just created

```python
...
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'my_database_name',
        'USER': 'my_username',
        'PASSWORD': 'my_password',
        'HOST': 'localhost',
        'PORT': '',
    }
}
...
```

>WARNING: With debug turned off Django won't handle static files (CSS & JS) for you any more - your production web server should take care of that.


#### 7. Create the background_tasks debug log folder and file. If something goes wrong during a vulnerability scan, you might want to have a look in this file.

```bash
# from dependency-scanner/ folder
$ mkdir logs/
$ touch logs/background_tasks.log
```

#### 8. Make migrations & migrate database.

```bash
# from dependency-scanner/ folder
$ python manage.py makemigrations
$ python manage.py migrate
```

#### 9. Start django server.

```bash
$ python manage.py runserver
```
To choose the ip and port for your webapp, you can simply add [ip_address]:[port]

```bash
$ python manage.py runserver 192.168.0.1:8000
```

#### 10. This projects uses Django's background tasks. Open a new terminal and start the tasks processing daemon:

```bash
$ python manage.py process_tasks
```

#### 11. You are ready to scan repositories! Navigate to http://[ip_address]:[port] with your web browser

Usage
=====

## a. New scan

To scan a new repository, choose a name, paste the link to your repository and click on "START SCANNING".

>As of today, only the following projects are supported
>- Python projects with a valid requirements.txt file at the root of the repository
>- Ruby projects with a valid Gemfile and Gemfile.lock at the root of the repository
>- Public git repositories

## b. In progress

On this page, your will find the scans that are currently in progress. When a scan disappears from this page, it means that it is done. Go to History to get the scan report.

## c. History

You will find here the list of all the scans performed by the app.
A red line means that vulnerabilities were found, a green one that nothing was found and a white line means that the scan is in progress.

Click on a line to see more.

## d. Scan scheduling

When you add a new repository, a scan will automatically be scheduled everyday at midnight.

You can manually trigger a new scan by clicking on "Restart scan now" on the scan report page.

You can also delete the scan, or disable the auto scheduling.

## e. CVEs, Dependencies and Exploits

Click on a CVE, a Dependency or an Exploit to see more.

Tests
=====

>Warning: If you used the sqlscript above during your installation, your MySQL user will only have access to the production database. Remember to set MySQL root user in your settings.py before running your tests.

You can run unittest with the command

```bash
# from depscan-scanner/ folder
$ python manage.py test
```

This will run tests on Circl, CpeGenerator, PythonParser, RubyParser and ExploitDatabase classes.

Tests are written in mainapp/test.py