# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.urls import reverse
from django.http import HttpResponse
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.core.exceptions import ObjectDoesNotExist
from background_task.models import Task

from mainapp.forms import ScanForm
from mainapp.models.scan import Scan, Dependency
from mainapp.models.cve import Cve
from mainapp.tools.tasks import scan_init, delete_repo

from shutil import rmtree
from os import path

def home(request):
    """ Home page - Scan creation """
    # If request is a post request, check user input
    if request.method == "POST":
        # Check input
        form = ScanForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            repo_link = form.cleaned_data['repo_link']
            # Create new Scan
            scan = Scan()
            scan.name = name
            scan.repository = repo_link
            scan.state = "in_progress"
            scan.random_id = get_random_string(length=12)
            # Midnight today
            midnight_today = timezone.datetime(timezone.now().year, timezone.now().month, timezone.now().day)
            scan.next_scan = midnight_today + timezone.timedelta(days=1)
            scan.save()
            # Schedule background task every day at midnight
            scan_init(scan.id, repeat=86400, schedule=midnight_today)
            # Redirect to in_progress page
            return redirect("in_progress_id", scan.id)

    return render(request, "mainapp/home.html")

def in_progress(request, scan_id=0):
    """ Display scans that are currently in progress """
    scans_in_progress = []
    if scan_id == 0:
        scans_in_progress = Scan.objects.filter(state="in_progress")
    else:
        scan = Scan.objects.get(pk=scan_id)
        if scan.state == "done" or scan.state == "error":
            return redirect("scan_report", scan.id)
        scans_in_progress = [scan]
    
    return render(request, 'mainapp/in_progress.html', {"scans": scans_in_progress})

def restart_scan(request, scan_id):
    """ Execute a dependency scan now """
    try:
        scan = Scan.objects.get(pk=scan_id)
    except ObjectDoesNotExist:
        return HttpResponse(status=500)
    scan.state = "in_progress"
    scan.dependency_set.all().delete()
    scan.scantask_set.all().delete()
    scan.save()
    # Reschedule a dependency scan
    scan_init(scan_id)
    # Redirect to in_progress page
    return redirect("in_progress_id", scan.id)

def enable_autoscan(request, scan_id):
    """ Restart a scan every day at midnight """
    try:
        scan = Scan.objects.get(pk=scan_id)
    except ObjectDoesNotExist:
        return HttpResponse(status=500)
    # Delete all previous autoscan
    Task.objects.filter(task_name="mainapp.tools.tasks.scan_init",
                        task_params="[["+str(scan.id)+"], {}]",
                        repeat=86400).delete()
    # Midnight tomorrow
    midnight_tomorrow = timezone.datetime(timezone.now().year, timezone.now().month, timezone.now().day) + timezone.timedelta(days=1)
    # Reschedule a scan for midnight tomorrow
    scan_init(scan.id, repeat=86400, schedule=midnight_tomorrow)
    scan.next_scan = midnight_tomorrow
    scan.save()
    return redirect('scan_report', scan.id)

def disable_autoscan(request, scan_id):
    """ Disable scan auto-rescheduling """
    try:
        scan = Scan.objects.get(pk=scan_id)
    except ObjectDoesNotExist:
        return HttpResponse(status=500)
    Task.objects.filter(task_name="mainapp.tools.tasks.scan_init",
                        task_params="[["+str(scan.id)+"], {}]",
                        repeat=86400).delete()
    # Update user config
    scan.next_scan = None
    scan.save()
    return redirect('scan_report', scan.id)

def delete_scan(request, scan_id):
    """ Delete a scan and stop any dependency scan """
    try:
        scan = Scan.objects.get(pk=scan_id)
    except ObjectDoesNotExist:
        return HttpResponse(status=500)
    # Delete background_task
    bg_task = Task.objects.filter(task_params="[["+str(scan.id)+"], {}]").delete()
    # Delete repository folder
    delete_repo(scan.get_repo_directory())
    # Delete scan
    scan.dependency_set.all().delete()
    scan.scantask_set.all().delete()
    scan.delete()
    # Redirect to in_progress page
    return redirect("home")

def history(request):
    # Select all scans
    scans = Scan.objects.all()
    return render(request, 'mainapp/history.html', {"scans": scans})

def scan_report(request, scan_id):
    """ Show scan report """
    try:
        scan = Scan.objects.get(pk=scan_id)
        return render(request, 'mainapp/scan_report.html', {"scan": scan})
    except ObjectDoesNotExist:
        return HttpResponse(status=404)

def dependency(request, dep_id):
    """ Show dependency report """
    try:
        dep = Dependency.objects.get(pk=dep_id)
        return render(request, 'mainapp/dependency.html', {'dep': dep})
    except ObjectDoesNotExist:
        return HttpResponse(status=404)

def cve(request, cve_id):
    """ Show CVE report """
    try:
        cve = Cve.objects.get(pk=cve_id)
        return render(request, 'mainapp/cve_report.html', {'cve': cve})
    except ObjectDoesNotExist:
        return HttpResponse(status=404)