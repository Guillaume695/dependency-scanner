# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from mainapp.models.scan import Cpe
from mainapp.tools.circl import Circl

class CpeGenerator:
    
    def compute_cpes(self, scan):
        """ Create a list of possible CPEs for each dependency of scan
        
        This method adds in database every possible CPEs for each dependency"""
        
        # Get the list of all language matching vendors
        circl = Circl()
        matching_vendors = circl.search_vendor(scan.language)
        
        # For each dependency, search new vendors et generate CPEs
        for dep in scan.dependency_set.all():
            # Add dep matching vendor
            matching_vendors_dep = matching_vendors + circl.search_vendor(dep.name)

            # For each vendor add possible cpe
            for vendor in matching_vendors_dep:
                cpe = Cpe()
                cpe.part = 'a'
                cpe.vendor = vendor
                cpe.product = dep.name
                cpe.version = dep.version
                cpe.dep = dep
                cpe.save()
        return