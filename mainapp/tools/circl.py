# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from requests import request, get
from mainapp.models.cve import Cve, CveVulnerableConfiguration, CveReference, CveNessus

class Circl:
    """ This class can be used to interact with https://cve.circl.lu web api """
    
    def get(self, url):
        full_url = "https://cve.circl.lu/api"+url
        try:
            response = get(full_url).json()
        except ValueError:
            response = None
        return response

    def get_all_vendors(self):
        """ Get the list of all possible vendors on https://cve.circl.lu
        Returns a list of string """
        response = self.get('/browse')
        if response == None:
            return []
        else:
            return response['vendor']
    
    def search_vendor(self, vendor_pattern):
        """ Search all vendors that matches vendor_pattern
        Returns a list of string """
        all_vendors = self.get_all_vendors()
        matching_vendors = [vendor for vendor in all_vendors if vendor_pattern in vendor]
        return matching_vendors

    def cve_search(self, scan):
        """ Search for CVE in CIRL database """
        # For each dependence, and for each CPE, search for a CVE
        for dep in scan.dependency_set.all():
            for cpe in dep.cpe_set.all():
                # print("Searching CVE for "+cpe.cpe_string())
                cves = self.get('/cvefor/'+cpe.cpe_string())

                # Parse each CVE into a Cve object, then save in database
                for cve in cves:
                    # Check if we haven't found this CVE for this dependence yet
                    new_cve = True
                    if 'id' in cve:
                        for cve_tmp in dep.cve_set.all():
                            if cve['id'] == cve_tmp.identifier:
                                new_cve = False
                    else:
                        new_cve = False
                    if not(new_cve):
                        continue

                    # Unfortunately, we can't use django's form feature as the output of CIRCL isn't Django friendly
                    # We have to parse everything manually

                    # Create Cve object
                    cve_db = Cve()
                    cve_db.identifier = cve['id'] if 'id' in cve else 'Empty'
                    cve_db.cvss_time = cve['cvss-time'] if 'cvss-time' in cve else 'Empty'
                    cve_db.published = cve['Published'] if 'Published' in cve else 'Empty'
                    cve_db.modified = cve['Modified'] if 'Modified' in cve else 'Empty'
                    if 'access' in cve:
                        cve_db.access_authentication = cve['access']['authentication'] if 'authentication' in cve['access'] else 'Empty'
                        cve_db.access_complexity = cve['access']['complexity'] if 'complexity' in cve['access'] else 'Empty'
                        cve_db.access_vector = cve['access']['vector'] if 'vector' in cve['access'] else 'Empty'
                    else:
                        cve_db.access_authentication = 'Empty'
                        cve_db.access_complexity = 'Empty'
                        cve_db.access_vector = 'Empty'
                    if 'impact' in cve:
                        cve_db.availability = cve['impact']['availability'] if 'availability' in cve['impact'] else 'Empty'
                        cve_db.confidentiality = cve['impact']['confidentiality'] if 'confidentiality' in cve['impact'] else 'Empty'
                        cve_db.integrity = cve['impact']['integrity'] if 'integrity' in cve['impact'] else 'Empty'
                    else:
                        cve_db.availability = 'Empty'
                        cve_db.confidentiality = 'Empty'
                        cve_db.integrity = 'Empty'
                    cve_db.last_modified = cve['last-modified'] if 'last-modified' in cve else 'Empty'
                    cve_db.summary = cve['summary'] if 'summary' in cve else 'Empty'
                    cve_db.cwe = cve['cwe'] if 'cwe' in cve else 'Empty'
                    cve_db.cvss = cve['cvss'] if 'cvss' in cve else 'Empty'
                    cve_db.cpe = cpe
                    cve_db.dependency = dep
                    cve_db.scan = dep.scan
                    cve_db.save()

                    # Other vulnerable configuration
                    if 'vulnerable_configuration' in cve:
                        for vuln_conf in cve['vulnerable_configuration']:
                            vuln_conf_db = CveVulnerableConfiguration()
                            vuln_conf_db.identifier = vuln_conf['id'] if 'id' in vuln_conf else 'Empty'
                            vuln_conf_db.title = vuln_conf['title'] if 'title' in vuln_conf else 'Empty'
                            vuln_conf_db.cve = cve_db
                            vuln_conf_db.save()
                    
                    # CVE references
                    if 'references' in cve:
                        for url in cve['references']:
                            cve_ref_db = CveReference()
                            cve_ref_db.url = url
                            cve_ref_db.cve = cve_db
                            cve_ref_db.save()
                    
                    # Nessus plugin
                    if "nessus" in cve:
                        for plugin in cve['nessus']:
                            cve_nessus_db = CveNessus()
                            cve_nessus_db.nasl_family = plugin['NASL family'] if 'NASL family' in plugin else 'Empty'
                            cve_nessus_db.nasl_id = plugin['NASL id'] if 'NASL id' in plugin else 'Empty'
                            cve_nessus_db.description = plugin['description'] if 'description' in plugin else 'Empty'
                            cve_nessus_db.last_seen = plugin['last seen'] if 'last seen' in plugin else 'Empty'
                            cve_nessus_db.modified = plugin['modified'] if 'modified' in plugin else 'Empty'
                            cve_nessus_db.plugin_id = plugin['plugin id'] if 'plugin id' in plugin else 'Empty'
                            cve_nessus_db.published = plugin['published'] if 'published' in plugin else 'Empty'
                            cve_nessus_db.reporter = plugin['reporter'] if 'reporter' in plugin else 'Empty'
                            cve_nessus_db.source = plugin['source'] if 'source' in plugin else 'Empty'
                            cve_nessus_db.title = plugin['title'] if 'title' in plugin else 'Empty'
                            cve_nessus_db.cve = cve_db
                            cve_nessus_db.save()
                
                # print(str(len(cves))+" vulnerabilities found for "+cpe.cpe_string())
        return