# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from background_task import background
from background_task.models import Task
from django.utils import timezone

from mainapp.models.scan import Scan, ScanTask
from mainapp.debug import Debug
from mainapp.tools.depparser import get_parser
from mainapp.tools.cpegenerator import CpeGenerator
from mainapp.tools.circl import Circl
from mainapp.tools.exploitdatabase import ExploitDatabase

from git import Repo
from git.exc import GitCommandError
from os import path
from shutil import rmtree
from time import sleep

@background(schedule=1)
def scan_init(scan_id):
    """ Step 1: Clone provided repo """
    # Select scan in database
    scan = Scan.objects.get(pk=scan_id)
    # Debug
    scan_task = ScanTask()
    debug = Debug("git", 3)
    message = "Cloning repository "+scan.repository
    scan_task.new_task(scan, message)
    debug.info(message)

    # Step 1: Clone provided repo
    # Empty repository folder if it already exists
    if path.isdir(scan.get_repo_directory()):
        rmtree(scan.get_repo_directory())
    try:
        # Clone repository
        Repo.clone_from(scan.repository, scan.get_repo_directory())
    except GitCommandError as git_error:
        # Bad request
        scan.state = "error"
        scan.next_scan = None
        scan.save()
        scan_task.state = "error"
        scan_task.save()
        debug.error("Cloning failed with status: "+str(git_error.status))
        return

    # Debug
    scan_task.task_done()
    debug.info("Repository cloned")
    # Creating next ScanTask (debug)
    scan_task = ScanTask()
    scan_task.new_task(scan, "Creating dependency list for "+scan.name)

    # Schedule next task: dependency file parsing
    scan_parse_dependency(scan.id)

@background(schedule=1)
def scan_parse_dependency(scan_id):
    """ Step 2: Find & parse dependency file """

    # Select scan in database
    scan = Scan.objects.get(pk=scan_id)
    # Debug
    debug = Debug("dependency", 3)
    # Get last task
    scan_task = ScanTask.objects.filter(scan_id=scan.id).last()
    debug.info("Creating dependency list for "+scan.name+" ...")

    # Step 2: Find & parse dependency file
    # Get parser according to the repo's language
    dep_parser = get_parser(scan)
    if dep_parser == None:
        return
    debug.info("Project language is "+scan.language)
    # Cleanup dependencies (to start from scratch)
    scan.dependency_set.all().delete()
    # Parse dependencies
    dep_parser.parse_project_dependencies(scan)

    # Debug
    scan_task.task_done()
    debug.info("Dependency list done!")
    # Creating next ScanTask (debug)
    scan_task = ScanTask()
    scan_task.new_task(scan, "Generating CPE ids")

    # Schedule next task: CPE generation
    scan_compute_cpe(scan.id)

@background(schedule=1)
def scan_compute_cpe(scan_id):
    """ Step 3: Compute CPEs for each dependency """

    # Select scan in database
    scan = Scan.objects.get(pk=scan_id)
    # Debug
    debug = Debug("cpe", 3)
    # Get last task
    scan_task = ScanTask.objects.filter(scan_id=scan.id).last()
    debug.info("Generating CPE ids for "+scan.name+" ...")

    # Step 3: compute CPEs for each dependency found
    # Cleanup all cpes (to start from scratch)
    for dep in scan.dependency_set.all():
        dep.cpe_set.all().delete()
    # Get cpe generator according to the repo's language 
    cpe_generator = CpeGenerator()
    cpe_generator.compute_cpes(scan)
    scan.save()

    # Debug
    scan_task.task_done()
    debug.info("CPE list generated!")
    # Creating next ScanTask (debug)
    scan_task = ScanTask()
    scan_task.new_task(scan, "Looking for CVEs in CIRCL database")

    # Schedule next task: CVE search in CIRCL database
    scan_cve_search(scan.id)

@background(schedule=1)
def scan_cve_search(scan_id):
    """ Step 4: Search for CVEs in CIRCL database """

    # Select scan in database
    scan = Scan.objects.get(pk=scan_id)
    # Debug
    debug = Debug("cve_search", 3)
    # Get last task
    scan_task = ScanTask.objects.filter(scan_id=scan.id).last()
    debug.info("Looking for CVEs in CIRCL database for "+scan.name+" ...")

    # Step 4: Search for CVEs in CIRCL database
    # Cleanup all cves (to start from scratch)
    scan.cve_set.all().delete()
    # Search for CVEs
    circl = Circl()
    circl.cve_search(scan)

    # Debug
    scan_task.task_done()
    debug.info("CVE search done!")
    # Creating next ScanTask (debug)
    scan_task = ScanTask()
    scan_task.new_task(scan, "Looking for exploits in ExploitDb database")

    # Schedule next task: search for exploitdb exploits
    scan_exploitdb(scan.id)

@background(schedule=1)
def scan_exploitdb(scan_id):
    """ Step 5: Search for exploits in ExploitDb """

    # Select scan in database
    scan = Scan.objects.get(pk=scan_id)
    # Debug
    debug = Debug("exploitdb_search", 3)
    # Get last task
    scan_task = ScanTask.objects.filter(scan_id=scan.id).last()
    debug.info("Looking for exploits in ExploitDb database for "+scan.name+" ...")

    # Step 4: Search for CVEs in CIRCL database
    # Cleanup all exploits (to start from scratch)
    scan.exploitdb_set.all().delete()
    # Search for exploits
    exploitdb = ExploitDatabase()
    exploitdb.search_exploits(scan)

    # Debug
    scan_task.task_done()
    debug.info("ExploitDb search done!")

    # Cleanup
    # Set every tasks as done
    for task in scan.scantask_set.all():
        task.task_done()
    # Set scan status as done
    scan.state = "done"
    scan.last_scan = timezone.now()
    # Select next scan_init task to get the next scan date
    next_scan_init = Task.objects.filter(task_name="mainapp.tools.tasks.scan_init",
                                        task_params="[["+str(scan.id)+"], {}]",
                                        repeat=86400).first()
    if next_scan_init != None:
        scan.next_scan = next_scan_init.run_at
    else:
        scan.next_scan = None
    scan.save()

@background(schedule=1)
def delete_repo(fullpath):
    """ Delete the folder 'fullpath' and its content """
    # For security reasons, django is run as www-data who doesn't have the permission to delete a folder in mainapp/static/*
    # Use a background_task to do that
    if path.isdir(fullpath):
        rmtree(fullpath)