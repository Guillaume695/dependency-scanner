# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from mainapp.models.scan import Dependency

from abc import ABCMeta, abstractmethod
from os import path
from subprocess import check_output, CalledProcessError
from shlex import split
from distutils.version import LooseVersion
import re

def get_parser(scan):
    """ Find dependency file and choose parser """
    if path.isfile(scan.get_repo_directory()+"/requirements.txt") or path.isfile(scan.get_repo_directory()+"/requirements-dev.txt"):
        scan.language = "python"
        scan.save()
        return PythonParser()
    elif path.isfile(scan.get_repo_directory()+"/Gemfile.lock"):
        scan.language = "ruby"
        scan.save()
        return RubyParser()
    else:
        # For now the project doesn't handle any other dependency manager
        scan.state = "Error - dependency file not found"
        scan.save()
        return None

class DepParser:
    __metaclass__ = ABCMeta
    
    @abstractmethod
    def parse_project_dependencies(self, scan):
        """ Read project dependency file
        
        This method should create the dep_list of parse_dependencies and call it. """
        pass

    @abstractmethod
    def parse_dependencies(self, scan, dep_list, parent_dependency=None):
        """ Extract dependency name & version from dep_list and create
        
        This method should call get_recursive_dependencies and fill_last_upstream_version when a new dependency is added in database. """
        pass
    
    @abstractmethod
    def get_recursive_dependencies(self, dependency):
        """ Fetch dependency's dependencies
        
        This method should call parse_dependencies on all recursive dependencies found, with the correct parent_dependency parameter. """
        pass
    
    @abstractmethod
    def fill_last_upstream_version(self, dependency):
        """ Fetch dependency's last upstream version
        
        This method should fill the upstream_version attribute of dependency """
        pass

class PythonParser(DepParser):
    
    def parse_project_dependencies(self, scan):
        """ Read requirements.txt file """
        if path.isfile(scan.get_repo_directory()+"/requirements.txt"):
            dep_file = open(scan.get_repo_directory()+"/requirements.txt","r")
        elif path.isfile(scan.get_repo_directory()+"/requirements-dev.txt"):
            dep_file = open(scan.get_repo_directory()+"/requirements-dev.txt","r")
        dep_file_raw = dep_file.readlines()
        dep_file.close()
        self.parse_dependencies(scan, dep_file_raw)
        return

    def parse_dependencies(self, scan, dep_list, parent_dependency=None):
        """ Extract dependency name & version from dep_list """
        for line in dep_list:
            if "==" in line:
                separator = "=="
            elif ">=" in line:
                separator = ">="
            elif "<=" in line:
                separator = "<="
            else:
                separator = "=="
            splitted_line = line.split(separator)
            if line[0] == "#" or line == "\n":
                continue
            name = str(splitted_line.pop(0)).rstrip()
            if len(splitted_line) >= 1:
                version = re.search('\d+[.\d+]*', splitted_line[0])
                if version != None:
                    version = str(version.group())
                else:
                    version = ''
            else:
                version = ''
            
            # Create new dependency
            (dep, new_dep) = Dependency.objects.get_or_create(scan_id=scan.id, name=name)
            if parent_dependency != None:
                dep.parent_dep = parent_dependency
            if new_dep:
                dep.version = version
                # print (dep.name, dep.version)
                self.fill_last_upstream_version(dep)
                # Fetch dependencies recursively
                self.get_recursive_dependencies(dep)
            else:
                # If there already is a dependency with that name, check the versions to only keep the last one
                if dep.version != '' and version != '':
                    if LooseVersion(dep.version) < LooseVersion(version):
                        dep.version = version
                        # print (dep.name, dep.version)
                        # Delete the dependencies the old version depended on
                        dep.dependency_set.all().delete()
                        # Recreate the list of dependencies this dependency depends on
                        self.fill_last_upstream_version(dep)
                        # Fetch dependencies recursively
                        self.get_recursive_dependencies(dep)
            dep.save()
        return

    def get_recursive_dependencies(self, dependency):
        """ Fetch dependency's dependencies """
        # TODO: create the list of depencies of "dependency".
        # IDEA: a) create a virtualenv
        #       b) install "dependency" in that virtualenv
        #       c) subprocess -> call pip freeze
        #       d) call recursively parse_dependencies with the list of dependencies obtained at step c)
        pass

    def fill_last_upstream_version(self, dependency):
        """ Fetch dependency's last upstream version """
        try:
            yolk_output = check_output(split('yolk -V '+str(dependency.name))).decode()
            version = re.search('\d+[.\d+]*', yolk_output.split('\n')[0])
            if version != None:
                dependency.upstream_version = str(version.group())
            else:
                dependency.upstream_version = ''
            if dependency.version == '':
                dependency.version = dependency.upstream_version
        except CalledProcessError:
            dependency.upstream_version = "Not found"
        return

class RubyParser(DepParser):

    def parse_project_dependencies(self, scan):
        """ Read Gemfile.lock file """
        dep_file = open(scan.get_repo_directory()+"/Gemfile.lock","r")
        dep_file_raw = dep_file.readlines()[3::]
        dep_file.close()
        self.parse_dependencies(scan, dep_file_raw)
        return

    def parse_dependencies(self, scan, dep_list, parent_dependency=None):
        """ Read every lines of Gemfile.lock and extract dependency name & version """
        skip = False
        for line in dep_list:
            if skip or line[0:4] == 'Gem ':
                continue
            first_word = re.search('\w+', line)
            if first_word == None:
                continue
            
            if first_word.group() == "PLATFORMS":
                skip = True
                continue
            
            splitted_line = line[first_word.start()::].split(' (')
            if len(splitted_line) == 0:
                continue
            
            name = str(splitted_line.pop(0)).rstrip()
            if len(splitted_line) > 0:
                version = re.search('\d+[.\d+]*', splitted_line[0])
                if version != None:
                    version = str(version.group())
                else:
                    version = ''
            else:
                version = ''
            
            # Create new dependency
            (dep, new_dep) = Dependency.objects.get_or_create(scan_id=scan.id, name=name)
            if parent_dependency != None:
                dep.parent_dep = parent_dependency
            if new_dep:
                dep.version = version
                # print (dep.name, dep.version)
                self.fill_last_upstream_version(dep)
                # Fetch dependencies recursively (Commented for now because too slow)
                # self.get_recursive_dependencies(dep)
            else:
                # If there already is a dependency with that name, check the versions to only keep the last one
                if dep.version != '' and version != '':
                    if LooseVersion(dep.version) < LooseVersion(version):
                        dep.version = version
                        # print (dep.name, dep.version)
                        # Delete the dependencies the old version depended on
                        dep.dependency_set.all().delete()
                        # Recreate the list of dependencies this dependency depends on
                        self.fill_last_upstream_version(dep)
                        # Fetch dependencies recursively (Commented for now because too slow)
                        # self.get_recursive_dependencies(dep)
            dep.save()
        return

    def get_recursive_dependencies(self, dependency):
        """ Fetch dependency's dependencies """
        # TODO: This method works but 'gem dependency' takes too much time ...
        try:
            gem_output = check_output(split('gem dependency '+dependency.name+' --remote --version="'+dependency.version+'"')).decode()
            dependency_list = gem_output.split('\n')
            first_line = dependency_list.pop(0)
            # Sometimes gem dependency gives multiple results, which means it didn't find the exact gem we asked for.
            # Only use the output if the first has the correct Gem name and version.
            if str(first_line) == "Gem "+dependency.name+"-"+dependency.version:
                self.parse_dependencies(dependency.scan, dependency_list, dependency)
        except CalledProcessError:
            pass
        return

    def fill_last_upstream_version(self, dependency):
        """ Fetch dependency's last upstream version """
        try:
            gem_output = check_output(split("gem  list ^"+dependency.name+"$ --remote --all")).decode()
            gem_output_splitted = gem_output.split(' ')
            if len(gem_output_splitted) > 0:
                version = re.search('\d+[.\d+]*', gem_output_splitted[1])
            else:
                version = None
            if version != None:
                dependency.upstream_version = str(version.group())
            else:
                dependency.upstream_version = ''
            if dependency.version == '':
                dependency.version = dependency.upstream_version
        except CalledProcessError:
            dependency.upstream_version = "Not found"
        return