# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^home$', views.home, name="home"),
    url(r'^in_progress$', views.in_progress, name="in_progress"),
    url(r'^in_progress_id/(\d+)$', views.in_progress, name="in_progress_id"),
    url(r'^restart_scan/(\d+)$', views.restart_scan, name="restart_scan"),
    url(r'^delete_scan/(\d+)$', views.delete_scan, name="delete_scan"),
    url(r'^history$', views.history, name="history"),
    url(r'^scan_report/(\d+)$', views.scan_report, name="scan_report"),
    url(r'^dependency/(\d+)$', views.dependency, name="dependency"),
    url(r'^autoscan/start/(\d+)$', views.enable_autoscan, name="enable_auto_scan"),
    url(r'^autoscan/stop/(\d+)$', views.disable_autoscan, name="disable_auto_scan"),
    url(r'^cve/(\d+)$', views.cve, name="cve"),
    url(r'', views.home),
]