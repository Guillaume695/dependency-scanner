# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from mainapp.models.scan import Scan, Dependency, Cpe

class Cve(models.Model):
    # Attributes
    identifier = models.CharField(max_length=256, null=True)
    cwe = models.CharField(max_length=256, null=True)
    published = models.CharField(max_length=512, null=True)
    cvss_time = models.CharField(max_length=512, null=True)
    cvss = models.CharField(max_length=512, null=True)
    modified = models.CharField(max_length=512, null=True)
    last_modified = models.CharField(max_length=512, null=True)
    access_authentication = models.CharField(max_length=512, null=True)
    access_complexity = models.CharField(max_length=512, null=True)
    access_vector = models.CharField(max_length=512, null=True)
    availability = models.CharField(max_length=512, null=True)
    confidentiality = models.CharField(max_length=512, null=True)
    integrity = models.CharField(max_length=512, null=True)
    summary = models.TextField(null=True)
    # Relationship
    scan = models.ForeignKey(Scan, on_delete=models.CASCADE, null=True)
    dependency = models.ForeignKey(Dependency, on_delete=models.CASCADE, null=True)
    cpe = models.ForeignKey(Cpe, on_delete=models.CASCADE, null=True)

class CveVulnerableConfiguration(models.Model):
    # Attributes
    identifier = models.CharField(max_length=512, null=True)
    title = models.CharField(max_length=512, null=True)
    # Relationship
    cve = models.ForeignKey("CVE", on_delete=models.CASCADE, null=True)

class CveReference(models.Model):
    # Attributes
    url = models.CharField(max_length=512, null=True)
    # Relationship
    cve = models.ForeignKey("CVE", on_delete=models.CASCADE, null=True)

class CveNessus(models.Model):
    # Attributes
    nasl_family = models.CharField(max_length=256, null=True)
    nasl_id = models.CharField(max_length=256, null=True)
    description = models.TextField(max_length=2048, null=True)
    last_seen = models.CharField(max_length=256, null=True)
    modified = models.CharField(max_length=256, null=True)
    plugin_id = models.CharField(max_length=256, null=True)
    published = models.CharField(max_length=256, null=True)
    reporter = models.CharField(max_length=256, null=True)
    source = models.CharField(max_length=256, null=True)
    title = models.CharField(max_length=256, null=True)
    # Relationship
    cve = models.ForeignKey("CVE", on_delete=models.CASCADE, null=True)