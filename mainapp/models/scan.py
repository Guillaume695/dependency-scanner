# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Scan(models.Model):
    # Attributes
    name = models.CharField(max_length=512, null=True)
    random_id = models.CharField(max_length=512, null=True)
    repository = models.CharField(max_length=512, null=True)
    language = models.CharField(max_length=512, null=True)
    state = models.CharField(max_length=512, null=True)
    date = models.DateTimeField(auto_now_add=True, auto_now=False, null=True)
    last_scan = models.DateTimeField(auto_now_add=False, auto_now=False, null=True)
    next_scan = models.DateTimeField(auto_now_add=False, auto_now=False, null=True)

    def get_repo_directory(self):
        """ Returns the directory in which the repo will be / is cloned. """
        return "mainapp/static/repos/"+str(self.random_id)
    
    def has_vulns(self):
        """ Returns True if a vulneranility was found for this project """
        has_vulns = False
        # Check each dependency
        for dep in self.dependency_set.all():
            if has_vulns:
                continue
            if (len(dep.cve_set.all()) > 0 or len(dep.exploitdb_set.all()) > 0):
                has_vulns = True
        return has_vulns
    
    def tot_cve(self):
        """ Returns the total number of CVEs found """
        tot_cve = 0
        for dep in self.dependency_set.all():
            tot_cve += len(dep.cve_set.all())
        return tot_cve
    
    def tot_exploit(self):
        """ Return the total number of exploits found """
        tot_exploit = 0
        for dep in self.dependency_set.all():
            tot_exploit += len(dep.exploitdb_set.all())
        return tot_exploit

class ScanTask(models.Model):
    # Attributes
    name = models.CharField(max_length=512, null=True)
    state = models.CharField(max_length=512, null=True)
    date = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)
    # Relationship
    scan = models.ForeignKey("Scan", on_delete=models.CASCADE, null=True)

    def new_task(self, scan, name):
        self.state = "in_progress"
        self.name = name
        self.scan = scan
        self.save()
        return
    
    def task_done(self):
        self.state = "done"
        self.save()
        return

class Dependency(models.Model):
    # Attributes
    name = models.CharField(max_length=512, null=True)
    version = models.CharField(max_length=512, null=True)
    upstream_version = models.CharField(max_length=512, null=True)
    # Relationships
    scan = models.ForeignKey("Scan", on_delete=models.CASCADE, null=True)
    parent_dep = models.ForeignKey("Dependency", on_delete=models.CASCADE, null=True)

    def has_vulns(self):
        return (len(self.cve_set.all()) > 0 or len(self.exploitdb_set.all()) > 0)

class Cpe(models.Model):
    # Attributes
    part = models.CharField(max_length=512, null=True)
    vendor = models.CharField(max_length=512, null=True)
    product = models.CharField(max_length=512, null=True)
    version = models.CharField(max_length=512, null=True)
    dep = models.ForeignKey("Dependency", on_delete=models.CASCADE, null=True)

    def cpe_string(self):
        return ":".join(["cpe:2.3", self.part, self.vendor, self.product, self.version])
