# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase

from mainapp.tools.circl import Circl
from mainapp.tools.cpegenerator import CpeGenerator
from mainapp.tools.depparser import PythonParser, RubyParser
from mainapp.tools.exploitdatabase import ExploitDatabase
from mainapp.models.scan import Scan, Dependency, Cpe

class CirclToolTestCase(TestCase):
    def test_get(self):
        circl = Circl()
        rep = circl.get('')
        self.assertIsNone(rep)
    
    def test_get_all_vendors(self):
        circl = Circl()
        vendors = circl.get_all_vendors()
        self.assertGreater(len(vendors), 1)
    
    def test_search_vendor(self):
        circl = Circl()
        matching_vendors = circl.search_vendor('python')
        for vendor in matching_vendors:
            self.assertEqual('python' in vendor, True)

    def test_cpe_search(self):
        scan = Scan.objects.create(name="Test")
        dep = Dependency.objects.create(scan=scan)
        cpe = Cpe.objects.create(part="a", vendor="djangoproject", product="django", version="1.8.2", dep=dep)
        circl = Circl()
        circl.cve_search(scan)
        self.assertGreater(len(scan.cve_set.all()), 1)
    
class CpeGeneratorTest(TestCase):
    def test_compute_cpes(self):
        scan = Scan.objects.create(name="Test", language="python")
        dep = Dependency.objects.create(scan=scan, name="django", version="1.8.2")
        cpegen = CpeGenerator()
        cpegen.compute_cpes(scan)
        self.assertGreater(len(dep.cpe_set.all()), 1)

class ExploitDbTest(TestCase):
    def test_search_exploits(self):
        scan = Scan.objects.create(name="Test", language="python")
        dep = Dependency.objects.create(scan=scan, name="django", version="1.8.2")
        exploitdb = ExploitDatabase()
        exploitdb.search_exploits(scan)
        self.assertGreaterEqual(len(scan.exploitdb_set.all()), 1)

class DepScannerTest(TestCase):
    def test_python_parse_dependencies(self):
        scan = Scan.objects.create(name="Test", language="python")
        py_parser = PythonParser()
        py_parser.parse_dependencies(scan, ["django==1.8.2", "django>=1.8.1", "pytz"])
        self.assertEqual(len(scan.dependency_set.all()), 2)
    
    def test_python_last_upstream_version(self):
        dep = Dependency.objects.create(name="django", version="1.8.2")
        py_parser = PythonParser()
        py_parser.fill_last_upstream_version(dep)
        self.assertNotEqual(dep.version, dep.upstream_version)

    def test_ruby_parse_dependencies(self):
        scan = Scan.objects.create(name="Test", language="ruby")
        ruby_parser = RubyParser()
        ruby_parser.parse_dependencies(scan, ["actionmailer (3.1.0)", "actionpack (= 3.1.0)", "builder (~> 3.0.0)", "coffee-script (>= 2.2.0)"])
        self.assertEqual(len(scan.dependency_set.all()), 4)
    
    def test_ruby_last_upstream_version(self):
        dep = Dependency.objects.create(name="json", version="1.7.7")
        ruby_parser = RubyParser()
        ruby_parser.fill_last_upstream_version(dep)
        self.assertNotEqual(dep.version, dep.upstream_version)