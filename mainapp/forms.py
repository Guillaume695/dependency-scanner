# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from mainapp.models.exploitdb import ExploitDb

class ScanForm(forms.Form):
    name = forms.CharField(max_length=512)
    repo_link = forms.CharField(max_length=512)