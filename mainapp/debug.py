# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime

import os

class Debug():
    def __init__(self, log_type, verbose):
        self.verbose = verbose
        self.log_type = log_type
    
    def info(self, message):
        self.write("Info", 3, message)
    
    def debug(self, message):
        self.write("Debug", 2, message)
    
    def error(self, message):
        self.write("Error", 1, message)
    
    def write(self, tag, verbose, message):
        log_str = datetime.now().__str__().split('.')[0]+" "+self.log_type+" "+tag+" "+message
        if self.verbose >= verbose:
            print(log_str)